import { Injectable } from '@angular/core';
import { CategoryPoi } from '../models/categoryPOI';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryPoiService {
  

  url: string = "https://furfooz.somee.com/api/category";

  context:BehaviorSubject<CategoryPoi[]>;

  constructor(private client : HttpClient) 
  {
    this.context = new BehaviorSubject<CategoryPoi[]>([]);
    this.refresh();
  }

  refresh() {
    this.client.get<CategoryPoi[]>(this.url).subscribe(data => this.context.next(data),error => this.context.error(error));
  }
}
