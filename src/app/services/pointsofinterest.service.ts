import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { poiObject } from '../models/poi';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PointsofinterestService {
  url: string = "https://furfooz.somee.com/api/pointinteret";
  context:BehaviorSubject<poiObject[]>

  constructor(private client : HttpClient) {
    this.context = new BehaviorSubject<poiObject[]>([]);
    this.refresh();
   }

   refresh(){
     this.client.get<poiObject[]>(this.url).pipe(map((x) => {
       for(let item of x)
       {
         item.IsSeen = false;
         item.AlreadyShown = false;
         item.CurrentPositionDist = 0;
       }
       return x;
     })).subscribe(data => this.context.next(data));
   }

}

