import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'sortedpoilist',
    loadChildren: () => import('./components/generics/sortedpoilist/sortedpoilist.module').then( m => m.SortedpoilistPageModule)
  },
  {
    path: 'genericpoi',
    loadChildren: () => import('./components/generics/genericpoi/genericpoi.module').then( m => m.GenericpoiPageModule)
  },
  {
    path: 'mapmenu',
    loadChildren: () => import('./components/generics/mapmenu/mapmenu.module').then( m => m.MapmenuPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./components/settings/settings.module').then( m => m.SettingsPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
