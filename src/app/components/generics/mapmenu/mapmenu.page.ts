import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { poiObject } from '../../../models/poi';
import { Storage } from '@ionic/storage';
import { GenericpoiPage } from '../genericpoi/genericpoi.page';

@Component({
  selector: 'app-mapmenu',
  templateUrl: './mapmenu.page.html',
  styleUrls: ['./mapmenu.page.scss'],
})
export class MapmenuPage implements OnInit {

  @Input() Eventlist : poiObject[];
  selectedLanguage: string;
  units : string;

  constructor(
    private modalcontroller : ModalController,
    private storage : Storage
  ) { }

  ngOnInit() {
    this.storage.get('selectedLanguages').then((x) => {this.selectedLanguage = x});
    this.storage.get('units').then((x) => {this.units = x});
    this.listSort()
  }

  close() {
    this.modalcontroller.dismiss();
  }

  listSort(){
    this.Eventlist = this.Eventlist.filter(x => x.IsDeleted == false);
    this.Eventlist.sort((a,b) => a.CurrentPositionDist > b.CurrentPositionDist ? 1 : -1);
  }

  getName(e: poiObject) : string {
    return this.selectedLanguage == 'fr' ? e.Name_fr : this.selectedLanguage == 'en' ? e.Name_en : e.Name_nl;
  }

  async poiModal(id : number) {
    const modal = await this.modalcontroller.create({
      component: GenericpoiPage,
      mode: 'ios',
      cssClass: 'my-custom-class',
      swipeToClose: true,
      componentProps: {'IdPOI' : id}
    });
    return await modal.present();
  }
}
