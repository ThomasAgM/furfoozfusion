import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GenericpoiPageRoutingModule } from './genericpoi-routing.module';

import { GenericpoiPage } from './genericpoi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GenericpoiPageRoutingModule
  ],
  declarations: [GenericpoiPage]
})
export class GenericpoiPageModule {}
