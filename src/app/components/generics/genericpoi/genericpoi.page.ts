import { Component, OnInit, Input } from '@angular/core';
import { poiObject } from '../../../models/poi';
import { ModalController } from '@ionic/angular';
import { PointsofinterestService } from '../../../services/pointsofinterest.service';

@Component({
  selector: 'app-genericpoi',
  templateUrl: './genericpoi.page.html',
  styleUrls: ['./genericpoi.page.scss'],
})
export class GenericpoiPage implements OnInit {

  @Input() IdPOI: number;
  poi : poiObject[];

  constructor(private modalcontroller : ModalController,
    private poiservice : PointsofinterestService
    ) { }

  ngOnInit() {
    this.poiservice.context.subscribe(data => {
      this.poi = data.filter(x => {
        return x.Id == this.IdPOI
      });
    })
  }

  close() {
    this.modalcontroller.dismiss();
  }

}
