import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PricesNInfosComponent } from './prices-ninfos.component';

describe('PricesNInfosComponent', () => {
  let component: PricesNInfosComponent;
  let fixture: ComponentFixture<PricesNInfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PricesNInfosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PricesNInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
